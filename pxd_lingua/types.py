from typing import Dict, Sequence


__all__ = 'Language', 'FallbackLanguages'

Language = str
FallbackLanguages = Dict[str, Sequence[str]]
