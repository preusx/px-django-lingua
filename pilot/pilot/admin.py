from django.contrib import admin

from pxd_lingua.admin import TranslationsInlineAdmin

from .models import Content, ContentTranslation


class ContentTranslationInlineAdmin(TranslationsInlineAdmin):
    model = ContentTranslation


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display = 'title', 'content', 'published_at'
    inlines = ContentTranslationInlineAdmin,
