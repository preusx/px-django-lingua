from django.db import models

from pxd_lingua import create_translation_model


__all__ = 'Content', 'ContentTranslation'


class Content(models.Model):
    title = models.CharField(max_length=40)
    content = models.TextField()

    published_at = models.DateTimeField()


ContentTranslation = create_translation_model(
    Content,
    fields=('title', 'content', 'published_at'),
)
