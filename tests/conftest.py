import sys
from django.conf import settings
from pathlib import Path
import environ


BASE_DIR = Path(__file__).parent

env = environ.Env()
env_file = str(BASE_DIR / '.env')
env.read_env(env_file)

sys.path.append(BASE_DIR.parent)


def pytest_configure():
    settings.configure(
        INSTALLED_APPS=[
            'tests.testproject',
            'pxd_lingua',

            'django.contrib.auth',
            'django.contrib.contenttypes',
        ],

        DATABASES={
            'default': env.db('DJANGO_DB_URL')
        },
    )
